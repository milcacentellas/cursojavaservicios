package com.fie.cognos.cursojava.services;
import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService 
public class HolaMundoService {
	@WebMethod
	public String saludar() {
		return "Hola Mundo";
	}

}
